# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* UI project for the Sy Business News Product. 
* 1.0

### How do I get set up? ###

* Extract to a clean folder, run "npm install" and "npm start" 

* There are sub Angular modules for latest new, news history and core components 

* Core components has an Angular service for fetching news bits using a REST call 
* Only news history uses REST,latest news uses a direct $http call to read a JSON file from the server 
* In the absence of the REST service deployment, uncomment Block 1 and comment Block 2 in app/biz-news-hist/biz-news-hist.component.js 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact