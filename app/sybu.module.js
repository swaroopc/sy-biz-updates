'use strict';

// Define the sybuApp module
angular.module('sybuApp', [

  'ngRoute',
  'ngAnimate',
  'ngMaterial', 
  'core',
  'bizNews',
  'bizNewsHist'

]);
