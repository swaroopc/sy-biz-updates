'use strict';

var app = angular.module('sybuApp');

app.config(['$locationProvider', '$routeProvider', '$mdThemingProvider',
  function config($locationProvider, $routeProvider, $mdThemingProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/latest', {
          template: '<biz-news></biz-news>'
        }).
        when('/history', {
          template: '<biz-news-hist></biz-news-hist>'
        }).
        otherwise('/latest');

      $mdThemingProvider.theme('default')
        .primaryPalette('teal')
        .accentPalette('blue-grey');
  }
]);

app.controller('BizNewsModuleController', function($scope, $mdSidenav, $mdDialog) {
  $scope.toggleAccordion = function() {
    $mdSidenav('news-accordian').toggle();
  };
});
