'use strict';

//Needs to be cleaned up or deleted since this controller has been moved to a component
describe('BUAppController', function() {

  beforeEach(module('sybuApp'));

  it('should create a `biznews` model with 4 news', inject(function($controller) {
    var scope = {};
    var ctrl = $controller('BizUpdatesController', {$scope: scope});

    expect(scope.biznews.length).toBe(4);
  }));

});
