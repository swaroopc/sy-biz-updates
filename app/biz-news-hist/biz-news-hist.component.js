angular.module('bizNewsHist').component('bizNewsHist',
{
  templateUrl: './biz-news-hist/biz-news-hist.template.html',

  controller: ['$http', '$resource', 'News', function BizUpdatesHistController($http, $resource, News) {
    var self = this;
    self.numberOfItemsToDisplay = 20;

//Block 1 - uncomment when there is no REST service call
/*
    $http.get('./biz-news-hist/biz-news-hist.json').then(function(response)
      {
        self.restResult = response.data;
      }
    );
*/

//Block 2 - comment when there is no REST service call
    News.get(function(restResponse) {
      self.restResult = restResponse;
    });

    function done() {
      //this.$broadcast('scroll.infiniteScrollComplete');
    }

    self.addMoreItem = function() {
      if (self.biznews.length > self.numberOfItemsToDisplay)
        self.numberOfItemsToDisplay += 20; // load 20 more items
      if (self.numberOfItemsToDisplay >= self.biznews.length)
        done(); // need to call this when finish loading more data
    }
  }]
});
