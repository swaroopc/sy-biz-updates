angular.module('bizNews').component('bizNews',
{
  templateUrl: './biz-news/biz-news.template.html',

  controller: ['$http', '$timeout', '$mdDialog', function BizUpdatesController($http, $timeout, $mdDialog) {
    var self = this;
    self.numberOfItemsToDisplay = 20;
    $http.get('./biz-news/biz-news.json').then(function(response)
      {
        self.biznews = response.data;
      }
    );
    function done() {
      this.$broadcast('scroll.infiniteScrollComplete');
    }
    self.addMoreItem = function() {
      if (self.biznews.length > self.numberOfItemsToDisplay)
        self.numberOfItemsToDisplay += 20; // load 20 more items
      if (self.numberOfItemsToDisplay >= self.biznews.length)
        done(); // need to call this when finish loading more data
    }

    self.infiniteItems = {
        numLoaded_: 0,
        toLoad_: 0,
        items:[],
        getItemAtIndex: function (index) {
            //window.alert("Get item called.");
            if (index >= this.numLoaded_) {
                //window.alert("Index is gt numLoaded_: i - " + index + " n - " + this.numLoaded_);
                this.fetchMoreItems_(index);
                return null;
            }
            //window.alert("Index is NOT gt numLoaded_: i - " + index + " n - " + this.numLoaded_ + " " + this.items[index].snippet);
            return this.items[index];
        },
        getLength: function () {
            //window.alert("Get lenght called. Returning: " + this.numLoaded_ + 5);
            return this.numLoaded_ + 5;
        },
        fetchMoreItems_: function (index) {
            //window.alert("Fetch more items called");
            if (this.toLoad_ < index) {
              //window.alert("toLoad_ is less than index: i - " + index + " t - " + this.toLoad_);
              this.toLoad_ += 20;

              /*
              $http.get('./biz-news/biz-news.json').success(function (data) {
                  var arr = data;
                  for (var i = 0; i < arr.length; i++) {
                      this.items.push(arr[i]);
                  }
                  this.numLoaded_ = this.toLoad_;
              }.bind(this));
              */

              $http.get('./biz-news/biz-news.json').then(angular.bind(this, function (obj) {
                  //window.alert(obj.data);
                  var fmf = this.items;
                  //window.alert("New numbers: n - " + this.numLoaded_ + " t - " + this.toLoad_);
                  $timeout(function() {
                    fmf = fmf.concat(obj.data);
                    this.numLoaded_ = this.toLoad_;
                  });
              }));

            }
        }
    };


    self.showAdvanced = function(ev) {
      $mdDialog.show({
        controller: self.this,
        templateUrl: '../core/infra/fs-overlay.tmplate.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: true
      })
      .then(function(answer) {
        self.status = 'You said the information was "' + answer + '".';
      }, function() {
        self.status = 'You cancelled the dialog.';
      });
    };

  }]
});
