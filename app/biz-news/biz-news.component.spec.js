'use strict';

describe('bizNews', function() {

  beforeEach(module('bizNews'));

  describe('BizUpdatesController' function() {

    it('should create a `biznews` model with 4 news', inject(function($componentController) {

      var ctrl = $componentController('bizNews');
      expect(scope.biznews.length).toBe(4);

    }));

});
