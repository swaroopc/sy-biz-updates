'use strict';

angular.
module('core.news').
factory('News', ['$resource',
  function($resource) {
    return $resource('http://localhost:8080/SyBizNewsService/newsservice/history/', {}, {
      query: {
        method: 'GET',
        params: {},
        isArray: true,
        responseType: "arraybuffer"
      }
    });
  }
]);
